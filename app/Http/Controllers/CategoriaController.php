<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::select('id', 'nombre')->orderBy('nombre', 'asc')->get();

        return $categorias;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Recoger datos
        $nombre = $request->input('nombre');

        // Asignar valores a objeto
        $categoria = new Categoria();
        $categoria->nombre = $nombre;

        try {
            // Guardar datos
            if($categoria->save()){
                $respuesta = true;
                $id = $categoria->id;
            } else {
                $respuesta = false;
                $id = null;
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $respuesta = false;
            $id = null;
        }
        

        return response()->json([
            'ok' => $respuesta,
            'id' => $id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::find($id);

        // Asignar nuevos datos
        $categoria->nombre = $request->input('nombre');

        try {
            if($categoria->save()){
                $respuesta = true;
            } else {
                $respuesta = false;
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $respuesta = false;
        }
        

        return response()->json([
            'ok' => $respuesta
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::find($id);

        try {
            $categoria->delete();
            $respuesta = true;
        } catch (\Illuminate\Database\QueryException $e) {
            $respuesta = false;
        }

        return response()->json([
            'ok' => $respuesta
        ]);
    }
}
