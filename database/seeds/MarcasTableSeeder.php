<?php

use Illuminate\Database\Seeder;

class MarcasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marcas')->insert([
        	'nombre' => 'Fultons',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Selloffice',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Art & Craft',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Proarte',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Patelwell',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Fixo',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Artel',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'JM',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Meiller',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Isofit',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Lewer',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Anca',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Nilsa',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Halley',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Torre',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Paradones',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Fosca',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Aron',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Hand',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Kadio',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Kenko',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Roka',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Paper Mate',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Bic',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Sony',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Monami',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Sabonis',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Sharpie',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Ballerina',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Kingston',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Avon',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Cadena',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('marcas')->insert([
        	'nombre' => 'Kodak',
        	'created_at' => date("Y-m-d H:i:s"),
        	'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
