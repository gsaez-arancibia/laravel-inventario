<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Agregamos nuestra ruta al controller de Marca
Route::resource('marcas', 'MarcaController');

// Agregamos nuestra ruta al controller de Categoria
Route::resource('categorias', 'CategoriaController');

// Agregamos nuestra ruta al controller de Proveedor
Route::resource('proveedores', 'ProveedorController');

// Agregamos nuestra ruta al controller de Producto
Route::resource('productos', 'ProductoController');
